package com.vehicle.monitor.rest.exception;

import java.util.Collections;
import java.util.List;

/**
 * User: Kamal Hossain
 * Date: 4/8/16
 */
public class ErrorMessage {
    private String code;
    private List<String> errors;

    public ErrorMessage() {
    }

    public ErrorMessage(List<String> errors, String code) {
        this.errors = errors;
        this.code = code;
    }

    public ErrorMessage(String error, String code) {
        this(Collections.singletonList(error), code);
    }

    public String getCode() {
        return code;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

}
