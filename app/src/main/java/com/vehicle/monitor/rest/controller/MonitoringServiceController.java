package com.vehicle.monitor.rest.controller;

import com.vehicle.monitor.rest.annotation.ApiVersion;
import com.vehicle.monitor.rest.dto.Vehicle;
import com.vehicle.monitor.rest.dto.Vehicles;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * User: Kamal Hossain
 * Date: 4/8/16
 */
@RestController
public class MonitoringServiceController {
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @ApiVersion(value = {"v1", "v2"})
    @RequestMapping(value = "/vehicle/put",
            method = RequestMethod.POST,
            headers = "Accept=application/xml",
            produces = {"application/xml"},
            consumes = {"application/xml"})
    public ResponseEntity<Object> createVehicleLog(@RequestBody Vehicles vehicles) {
        StringBuilder sbUrl = new StringBuilder();
        for (Vehicle vehicle : vehicles.getVehicles()) {
            sbUrl.append("http://localhost/gprmc/Data?1=1");
            sbUrl.append("&dev=" + vehicle.getDeviceId());
            sbUrl.append("&lat=" + vehicle.getEvent().getLatitude());
            sbUrl.append("&lon=" + vehicle.getEvent().getLongitude());
            sbUrl.append("&speed=" + vehicle.getEvent().getSpeed() * 3.6);
            sbUrl.append("&date=" + dateFormat.format(vehicle.getEvent().getTime()));
            sbUrl.append("&time=" + timeFormat.format(vehicle.getEvent().getTime()));
            String strUrl = sbUrl.toString();
        }
        return new ResponseEntity("OK", HttpStatus.OK);
    }
}
