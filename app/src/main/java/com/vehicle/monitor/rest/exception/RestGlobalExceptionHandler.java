package com.vehicle.monitor.rest.exception;


import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * User: Kamal Hossain
 * Date: 4/8/16
 */
@ControllerAdvice(basePackages = "com.cloudix.rest.controller")
public class RestGlobalExceptionHandler extends ResponseEntityExceptionHandler {


    public RestGlobalExceptionHandler() {
        super();
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage("Invalid Request Params", String.valueOf(status.value()));
        return new ResponseEntity(errorMessage, headers, status);
    }

}
