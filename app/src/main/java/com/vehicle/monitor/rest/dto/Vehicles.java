package com.vehicle.monitor.rest.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;

/**
 * User: Kamal Hossain
 * Date: 4/11/16
 */
@XmlRootElement(name = "vehicles", namespace = "http://www.cargoonline.ru/xsd/sync-1.0.xsd")
@XmlAccessorType(XmlAccessType.FIELD)
public class Vehicles {
    @XmlElement(name = "vehicle")
    List<Vehicle> vehicles;

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
