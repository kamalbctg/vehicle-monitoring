package com.vehicle.monitor.rest.dto;

import javax.xml.bind.annotation.*;

/**
 * User: Kamal Hossain
 * Date: 4/11/16
 */
@XmlRootElement(name = "vehicle")
@XmlAccessorType(XmlAccessType.FIELD)
public class Vehicle {
    @XmlAttribute
    private long deviceId;

    @XmlElement
    private Event event;

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
