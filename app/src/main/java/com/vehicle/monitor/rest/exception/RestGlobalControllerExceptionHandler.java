package com.vehicle.monitor.rest.exception;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User: Kamal Hossain
 * Date: 4/9/16
 */
@ControllerAdvice(basePackages = "com.cloudix.rest.controller")
public class RestGlobalControllerExceptionHandler {
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorMessage handleException(ResourceNotFoundException ex) {
        return new ErrorMessage("resource not found", String.valueOf(HttpStatus.NOT_FOUND.value()));
    }
}
