package com.vehicle.monitor.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * User: Kamal Hossain
 * Date: 4/9/16
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private Long resourceId;
    private String resourceType;

    public ResourceNotFoundException(Long resourceId, Class clz) {
        this.resourceId = resourceId;
        this.resourceType = clz.getSimpleName();
    }

    public Long getResourceId() {
        return resourceId;
    }

    public String getResourceType() {
        return resourceType;
    }
}
