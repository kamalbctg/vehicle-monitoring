package com.vehicle.monitor.rest.dto;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Kamal Hossain
 * Date: 4/11/16
 */
public class EventDateConverter extends XmlAdapter<String, Date> {
    //TDO : need to change with joda time
    final DateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public Date unmarshal(String dateStr) throws Exception {
        return ft.parse(dateStr);
    }

    @Override
    public String marshal(Date date) throws Exception {
        return ft.format(date);
    }
}
